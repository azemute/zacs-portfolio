# [My Portfolio](https://17c.ca)

## Purpose
The general purpose of this repository and project is multifaceted:

1. to have a place to store my photos and show them off to friends and family
1. to have a proof of concept for various serverless technologies
1. as a place to implement tooling ideas I have out-of-band
1. and to be able to do some programming where I normally don't get much opportunity to do that anymore

## Implementation

In principle the idea is as follows:

- The main site is purely stateless html5 and javascript
- It is deployed to an S3 bucket and fronted by an AWS CloudFront CDN
- New photos uploaded to the S3 bucket trigger python watch scripts to generate new index json files
- It uses BitBucket pipelines to automate deployments of the front-end components to the site


In general, I'm happy with how this works, and it's quite simple. I elected not to use any javascript frameworks because it was grossly unnecssary and overkill, and also permitted me to learn javascript directly at the most bare level.



![42](42.png)