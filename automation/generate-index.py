import json, boto3
from time import time

def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    webroot = s3.Bucket('17c.ca')
    
    bucket = event['Records'][0]['s3']['bucket']['name']
    prefix = event['Records'][0]['s3']['object']['key'].rsplit('/', 1)[0] + '/'
    
    if prefix == 'main/':
        print("this is the home page - bailing out")
        return False

    index = []
    exclude = ['', 'images.json']
    
    for obj in webroot.objects.filter(Prefix=prefix):
        if obj.key.replace(prefix,'') not in exclude:
            # TODO: check if there's a title available in the titles file
            index.append({'title': '', 'url': obj.key.replace(prefix,'')})
    
    s3object = s3.Object(bucket, prefix + 'images.json')
    s3object.put(ContentType='application/json', 
                 Body=(bytes(json.dumps(index).encode('UTF-8'))))
    
    cloudfront = boto3.client('cloudfront')
    cloudfront.create_invalidation(
        DistributionId='E19GW8O00KQCOK',
        InvalidationBatch={
            'Paths': {
                'Quantity': 1,
                'Items': [
                    '/'+prefix+'/*'
                ],
            }, 
            'CallerReference': str(time()).replace(".", "")
        }
    ) 
