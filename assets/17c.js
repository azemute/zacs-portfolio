/* HorizontalDivision creates a new horizontal Gallery object representing one or more pictures */
class HorizontalDivison {
    constructor(path, image_data) {
        this.base_path = path
        if (Array.isArray(image_data)) {
            this.img = []
            for (let idx in image_data) {
                this.img.push(new ImageObject(this.base_path, image_data[idx]))
            }
        } else {
            this.img = new ImageObject(this.base_path, image_data)
        }
    }
    create_division(type) {
        let article = document.createElement("section")
        article.classList.add("division")
        if (!Array.isArray(this.img)) {
            let image = this.img.create_img()
            article.classList.add("landscape")
            article.append(image)
        } else {
            // if the count is high enough, disable the exif data
            let disable_exif = false
            if (this.img.length > 6) {
                disable_exif = true
            }
            for (let idx in this.img) {
                let portrait = document.createElement("div")
                let image = this.img[idx].create_img(disable_exif)
                portrait.append(image)
                article.classList.add("portrait")
                portrait.classList.add("scale-" + this.img.length)
                article.append(portrait)
            }
        }
        return (article)
    }
}

/* GalleryImage can be one or more Images in an arrangement - adding the EXIF data to each, if requested */
class ImageObject {
    constructor(base_path, image) {
        this.id = image.url.split("/").slice(-1)[0]
        this.url = base_path + '/' + image.url
        this.meta = image.meta
        this.title = typeof(image.title) !== 'undefined' ? image.title : ""
    }
    create_img(exif_disabled = false) {
        if (exif_disabled == false) {
            this.get_exif()
        }
        let para = document.createElement('p')
        para.innerHTML = this.title
        let image = document.createElement('article')
        image.id = this.id
        image.style.backgroundImage = "url('" + this.url + "')"
        image.append(para)
        return (image)
    }
    
    /* 
        this does some exif validation
        sadly Canon and Pentax don't both use the same EXIF data layouts, so it's a bit of swiss cheese
    */
    validate_exif(details){
        /* TODO: cleanup other possible EXIF data here */
        if(typeof details.ExposureTime === "undefined"){ details.ExposureTime = "no data" }
            else if(details.ExposureTime.denominator > 1){ details.ExposureTime = details.ExposureTime.numerator + "/" + details.ExposureTime.denominator + "s" }
            else { details.ExposureTime = details.ExposureTime.numerator + "s" }
        
        if(typeof details.LensModel === "undefined"){ details.LensModel = "no data" }
            else { details.LensModel = details.LensModel.replace(/\.0/g, "") }
        
        if(typeof details.FNumber === "undefined"){ details.FNumber = "no data" }
            else { details.FNumber = "ƒ/" + details.FNumber.numerator / details.FNumber.denominator }
        
        if(typeof details.ISOSpeedRatings === "undefined"){ details.ISOSpeedRatings = "no data" }
            else { details.ISOSpeedRatings = "ISO " + details.ISOSpeedRatings }
        
        return details
    }
    
    get_exif() {
        let img_ctx = this
        let pr_im = new Image()
        pr_im.onload = function() {
            EXIF.getData(pr_im, function() {
                let details = img_ctx.validate_exif(EXIF.getAllTags(pr_im))
                console.log(details)
                let c_check = document.createElement("input")
                c_check.setAttribute("type", "checkbox")
                c_check.id = img_ctx.id + "-id"
                let c_detail_block = document.createElement("label")
                c_detail_block.classList.add("camera_details")
                c_detail_block.setAttribute("for", img_ctx.id + "-id")

                let c_model = document.createElement("span")
                c_model.classList.add("title")
                c_model.innerHTML = details.Model

                let c_details = document.createElement("span")
                c_details.classList.add("details")

                let c_lens_details = document.createElement("span")
                c_details.innerHTML = details.ExposureTime + " | " + details.FNumber + " | " + details.ISOSpeedRatings

                if (details.LensModel != "no data") {
                    c_details.innerHTML = details.LensModel + "<br>" + c_details.innerHTML
                }

                c_detail_block.append(c_model)
                c_detail_block.append(c_details)
                c_detail_block.append(c_check)
                c_detail_block.append(img_ctx.gen_data_overlay(details))
                document.getElementById(img_ctx.id).append(c_detail_block)
            })
        }
        pr_im.src = img_ctx.url
    }
    gen_data_overlay(exif_data) {
        /* TODO: validate data available rather than fail on an entry being missing */
        let data_overlay = document.createElement("div")
        data_overlay.classList.add("overlay")
        let o_title = document.createElement("h1")
        o_title.innerHTML = exif_data.Model

        let o_details = document.createElement("pre")

        function desc(title, item) {
            return title + ": " + item + "\n"
        }

        o_details.innerHTML = desc("Date", exif_data["DateTime"]) + 
            desc("Exposure Program", exif_data["ExposureProgram"]) +
            desc("Exposure Bias", exif_data["ExposureBias"]) +
            desc("Flash", exif_data["Flash"]) +
            desc("Metering Mode", exif_data["MeteringMode"])
        data_overlay.append(o_title)
        data_overlay.append(o_details)
        return data_overlay;
    }
    gen_lightbox() {
        return true;
    }
}


// cleanup the old divisions to repopulate the page
function clean_destroy(){
    const divisions = document.querySelectorAll('.division');
    for (let d of divisions) {
      d.remove();
    }
}

// url parameter magic
function get_url_lets() {
    let lets = {};
    let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        lets[key] = value;
    });
    return lets;
}

function get_url_param(parameter, defaultvalue){
    let urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = get_url_lets()[parameter];
    }
    return urlparameter;
}